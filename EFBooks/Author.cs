namespace Library;

public class Author{
    public string FirstName {get; private set;}
    public string LastName {get; private set;}
    public int AuthorID {get; private set;}
    public List<Book> Books {get;}

    public Author(string fname, string lname){
        FirstName = fname;
        LastName = lname;
        Books = new List<Book>();
    }

    //If we add a constructor, we must still declare an empty constructor for EF
    //It can be private though!
    private Author(){
        Books = new List<Book>();
    }

}