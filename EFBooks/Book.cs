namespace Library;

public class Book{
    public int BookID {get; private set;}
    public string Title {get; private set;}
    public string Genre {get; private set;}

    public int AuthorID {get; private set;}
    public Author Author {get; private set;}

    public Book(string title, string genre, Author author){
        Title = title;
        Genre = genre;
        Author = author;
    }

    //If we add a constructor, we must still declare an empty constructor for EF
    //It can be private though!
    private Book(){
    }
}