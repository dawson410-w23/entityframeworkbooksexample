﻿using Library;
using Microsoft.EntityFrameworkCore;

Console.WriteLine("Welcome to Library App!");
LibraryContext db = new LibraryContext();

Console.WriteLine("Would you like to add books? (y/n)");
while(Console.ReadLine().Equals("y")){
    Console.WriteLine("Enter book title");
    string title = Console.ReadLine();
    Console.WriteLine("Enter book genre");
    string genre = Console.ReadLine();
    Console.WriteLine("Enter author firstname");
    string fname = Console.ReadLine();
    Console.WriteLine("Enter author lastname");
    string lname = Console.ReadLine();

    Author a = new Author(fname, lname);

    Book b = new Book(title, genre, a);

    Console.WriteLine($"Adding book {title}");
    db.Add(b);
    db.SaveChanges();
    Console.WriteLine($"{title} added!");

    Console.WriteLine("Enter another book? (y/n)");
}


Console.WriteLine("Stored books:");

List<Book> booklist = db.Books
    .Include(book => book.Author) //Specifies that we want to load related data. Think of it as a join.
    .ToList<Book>();

foreach (var b in booklist)
{
    Console.WriteLine($"Book {b.BookID}: {b.Title} in genre {b.Genre}, written by {b.Author.FirstName} {b.Author.LastName}");
}






